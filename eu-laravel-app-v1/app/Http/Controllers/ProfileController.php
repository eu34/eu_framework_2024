<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Experience;


class ProfileController extends Controller
{
    //
    public function experience(){
        return "Profile Experiences";
    }

    //
    public function language(){
        // return "Profile Languages - ".$language;
        return view("language");
    }

    public function experience_add(){
        $experience =  new Experience();
        $experience->title = "Eurpean University";
        $experience->description = "Tutor";
        $experience->started_at = "2010-09-15";
        $experience->ended_at = "2011-09-15";
        $experience->save();
        return "Add new record!!";
    }

    public function experience_create(Request $request){
        // dd($request);
        
        // return "Create new record!!";

        $experience =  new Experience();
        $experience->title = $request->title;
        $experience->description = $request->description;
        $experience->started_at = $request->started_at;
        $experience->ended_at = $request->ended_at;
        $experience->save();
        return redirect()->route("gen");
    }
}

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>FORM</title>
</head>
<body>
    <h1>FORM</h1>
    <form action="{{route('exper_create')}}" method="POST">
        @csrf
        <div>
            Title - <input type="text" name="title">
        </div>
        <div>
            Description - <input type="text" name="description">
        </div>
        <div>
            Start - <input type="date" name="started_at">
        </div>
        <div>
            Ent - <input type="date" name="ended_at">
        </div>
        <div>
            <button>Add New Experience</button>
        </div>
    </form>
</body>
</html>
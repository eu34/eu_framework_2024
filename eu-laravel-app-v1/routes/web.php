<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {  return view('welcome'); })->name("gen");
Route::get('/education', function () {  return view('education'); })->name("edu");

Route::get('/experience', [App\Http\Controllers\ProfileController::class, 'experience'])->name("exper");
Route::get('/lang/{lang?}', [App\Http\Controllers\ProfileController::class, 'language_lavel'])->name("lang_lavel");
Route::get('/language', [App\Http\Controllers\ProfileController::class, 'language'])->name("lang");
Route::get('experience_form', function () {  return view('experience_form'); })->name("experience_form");
Route::get('/experience_add', [App\Http\Controllers\ProfileController::class, 'experience_add'])->name("exper_add");

Route::post('/experience_create', [App\Http\Controllers\ProfileController::class, 'experience_create'])->name("exper_create");




// Route::get('/language/{lang?}', function ($language="EN"){   
//     return $language;
// });




